> Start with Exalted 2nd Edition with 2.5 Errata, then add these additional house rules.

## General Rules

- If two powers (merit/artifact/charm/spell) enhance the same thing (e.g. dash/jump distance), remember that a 'tripling' is actually base +2base. So a charm that Triples movement speed, an artifact that Doubles movement speed, and a mutation that Doubles movement speed results in base +2baseCharm +1baseArtifact +1baseMutation for a result of 5x speed, not 12x as 1x3x2x2 would imply.
- Combos don't need to be defined with names or signature combo flares, there is no special visual effect
- There is no such thing as a Fivefold Harmonic Adapter
- There is no such thing as an Elemental Lens

### Anima Banners
The “bonfire” level anima display (where many exalts get free or discounted anima power) occur at 12+ motes rather than 11+. This way the anima banner increments in neat, even steps of four: none 0, twinkle 1-3, candle 4-7, torch 8-11, bonfire 12-15 and iconic 16+.

## Character Creation

### General Changes

- There are no bonus points. Instead starting characters get 100chargenXp.
- Willpower starts at the sum of your two highest virtues.
- Pick 2 _Caste_/_Favored_ Trait to be called your _Cardinal_ Traits (see below for more on _Cardinal_ Traits)
- Free dots cannot be used to go over a rating of 3 in _anything_, with the following exceptions:
    - Primary Virtue (drives your Great Curse) may be raised to 4 with free dots (see below for more on virtues)
    - _Cardinal_ Traits may be raised to 5 (see below for more on _Cardinal_ Traits)
    - Attributes points are uncapped, but at most two attributes, each in a different category, can have a value of 1

### Flaws, Merits, and Mutations

- Allowed only with ST approval at a cost of 2xp per 1pt
- Opportunities to gain Merits through gameplay might arise, but must be purchased if net (Merit-Flaw) cost is positive
- A negative (Merit - Flaw) cost is tracked, but you do not gain xp spendable towards anything other than future Merits
- Up to 20pt of Flaws can 'count' (again with ST approval) instead of the usual 10pt
- All Exalts have Merit: **Ambidextrous Merit** and the -2 DV penalty applied to readying a weapon mid flurry
- Flaw: **Diminished Touch** only gives 2pts and does not reduce wound penalties
- Flaw: **Greater Curse** can provide at most 2pts
- Merit: **Selective Conception** ... You can sense if copulation might sire offspring (no Maiden Tea taken). Normally fertility and conception is simulated with 1 die rolled after a night of copulation, conceiving on 3 or below. Multiple partners in a night give ST the choice of father. Fathers with this Merit, however, can elect to be rolled before others (if multiple, higher Essence wins, if a tie, roll off with a single die, highest goes first) and sire offspring on (10 + Essence)/2 or below at a cost of 1wp. Mothers can always guarantee conception or not regardless of if the Father has this Merit and tries to force conception.
- Merit: **Priest** 7pt ... don't abuse the wp and mote regen too much, your god might get annoyed
- Merit: **Danger Sense** 5pt does not help against surprise attacks fueled by Essence
- Merit: **Special Sense(Telepathy)** does not exist
- Merit: **Sorcery Focus** does not exist
- Merit: **Paragon of (Virtue)** doesn't exist, Virtues don't go above 5 ever by any means
- Merit: **(Virtue) Specialty** does not exist (exception: see Cardinal Traits note regarding Eternals)
- New Ability (first dot) cost with **Prodigy** Merit is 1xp (past xp refunded)
- Merit: **Terrestrial Bloodline** ... rewrite: non-terrestrials can buy Breeding background as if they are DBs. Kids roll to exalt as if you are still a mortal, but with Breeding's benefits. If Ess4+, failure on that roll [at conception] means they will be half-caste.

> Merit: **Divine Heritage**  
> **Cost**: 7pts  
> **Availability**: Exalts  
>
> Exalts sometimes descend from gods, demons, ghosts, fae, or other creatures with supernatural power. The player should work with the Storyteller to define the nature and thematics of the divine ancestor. The character can purchase charms as if via the Eclipse anima power that the Storyteller agrees fits their divine parent's thematics. A character's divine lineage may have other manifestations, which can be represented with Flaws or other Merits.

> Merit: **Extra Favored Patron**  
> **Cost**: 10pts  
> **Availability**: Green Sun Princes  
> 
> Your character favors charms of an additional Yozi. This Merit may only be purchased once.

### Health Levels

- Exalts get a number of free additional -4 health levels equal to (Stamina+Resistance+Essence).

### Unified Experience Costs and Training Times

```
Trait Increase                Cost (in xp)      Training Times
-------------------------------------------------------------------------------
Non-Favored Attribute         ( rating x4 )    ( rating ) months
Favored Attribute             ( rating x3 )    ( rating ) weeks
Cardinal Attribute            ( rating x3 )    ( rating ) days

Non-Favored Ability           ( rating x2 )    ( rating +1 ) weeks
Favored Ability               ( rating x2 )-1  ( rating +1 ) days
Cardinal Ability              ( rating x2 )-1  immediate

New Non-Favored Ability       3                3 weeks
New Favored Ability           3                3 days
New Cardinal Ability          1                immediate

Essence                       ( rating x8 )    ( rating ) months
Virtue                        ( rating x3 )    ( rating ) days
Willpower                     ( rating x1 )    ( rating ) days
Specialty                     3                ( rating +1 ) weeks

Non-Favored Charm             10               Sum( Minimum Traits ) days
Favored Charm                 8                Max( Minimum Traits ) days
Cardinal Charm                8                Min( Minimum Traits ) days
Type-Unique Charm             8                see source
Out-of-Type Charm (Fav/Un)    12 / 15          Sum( Minimum Traits ) weeks
Exceeding MA Charm (Fav/Un)   12 / 15          Sum( Minimum Traits ) weeks

Sorcery/Necromancy/Protocols  See Below        ( spell circle ) weeks
```

- Favored Spell Pricing:
    - Initiated into 3rdCirlce: 1stCircle 4xp, 2ndCircle 6xp, 3rdCircle 8xp
    - Initiated into 2ndCircle: 1stCircle 6xp, 2ndCircle 8xp
    - Initiated into 1stCircle: 1stCircle 8xp
- Non-Favored Spell Pricing:
    - Initiated into 3rdCirlce: 1stCircle 5xp, 2ndCircle 7xp, 3rdCircle 10xp
    - Initiated into 2ndCircle: 1stCircle 7xp, 2ndCircle 10xp
    - Initiated into 1stCircle: 1stCircle 10xp

### Cardinal Traits

- Pick 2 Caste/Favored Trait to be called your Cardinal Traits (see below for more on Cardinal Traits)
    - Solars, Abyssals, Sidereals, and Terrestrials all have Cardinal Abilities
    - Infernals get their Caste Yozi and 1 Favored Yozi as Cardinal Yozis
    - Lunars and Alchemicals get Cardinal Attributes
- Can learn Charms associated with this Trait requiring an Essence rating one higher than normal
    - Example: an Essence 3 solar could learn Charms for Cardinal Abilities that require Essence 4
- Can raise Trait to one above current essence rating once Essence 5
    - Example: an Essence 5 lunar could raise their Cardinal Attributes to 6
    - Lunar Charm **Impossible (Attribute) Improvement** stacks with this
    - Merit: **Legendary (Attribute)** stacks with this
    - Eternals can instead purchase Merit: **Virtue Specialty** a number of times equal to (Essence - 4)
    - Infernals can instead purchase **Heretical** Charms based off only their Cardinal Yozis one Essence higher than normal

### Excellencies

- First Excellency now subsumes Second Excellency (they are merged) and is renamed **Empowered (Trait) Excellency**. You can spend motes in any combination of the two options up to your normal mote caps.
- Third Excellency (and higher) are referred to by their subtitle, so Third is now **Resurgent (Trait) Excellency**, etc.
- You automatically get the merged XLNC in your Caste/Favored Traits that are rated 3 or higher
- Infernals get the merged XLNC for their Cardinal Yozis for free. Additionally, other Yozi XLNC purchases after 3rd stack are free.

### Virtues

- Daily willpower regen roll uses your Primary Virtue instead of always Conviction

### Free Charms

- You get 10charmXP instead of each 'free charm' slot, so for example Solars with 10 free charms get 100charmXP to spend on charms and spells. Spend that charmXp only on _only_ charms and spells.
- Any leftover small fragments (less than lowest charm/spell cost available to you) should be augmented by your 100chargenXP so as not to be wasted.

## Martial Arts

### Initiation Changes

- Upon Learning a CMA Form Charm and at least 4 CMA Charms:
    - You automatically learn the equivalent of **Swallowing the Lotus Root** (xp refunded if bought before this)
    - CMA Initiation Charms count as TMA Charms
- Upon Learning an SMA Form Charm, at least 4 SMA Charms, and a Complete CMA:
    - the cost, training time, and development time of CMA Charms is reduced to 3/4 normal (round up & past xp refunded)
- Upon Learning a Complete SMA:
    - the cost, training time, and development time of CMA Charms is reduced to 1/2 normal (round up & past xp refunded)

## Sorcery and Necromancy

### Combat Changes

- Shape Sorcery actions do not pull you out of combat time, instead Cast Sorcery happens immediately upon completion of all necessary Shape Sorcery actions. No Join Battle roll is necessary at the end of your Casting.
- You can use 1st and 2nd XLNCs to augment your casting with a 1wp surcharge to the first Shape Sorcery action for that spell
- You can use Reflexive Charms for Defense at a surcharge of 1m during Shape Sorcery actions
- At the end of each turn in which you used an XLNC or Reflexive Charm you must roll for distraction (Wits + Occult) at a difficulty of (1 + damage taken + motes spent). Motes pre-spent using Infinite Mastery and similar effects are not counted as 'motes spent' for this difficulty.
- XLNCs may be used towards distraction rolls without increasing the difficulty

### Sorcery Initiation Changes

- Upon initiating into Emerald Sorcery:
    - You automatically learn **Dispel Emerald Sorcery** for free (see Spell Changes below)
- Upon initiating into Sapphire Sorcery:
    - You automatically learn **Dispel Sapphire Sorcery** for free (see Spell Changes below)
    - The cost, training time, and research time for Emerald spells is reduced to 3/4 normal (round down & past xp refunded)
- Upon Initiating into Adamant Sorcery:
    - You automatically learn **Dispel Adamant Sorcery** for free (see Spell Changes below)
    - The cost, training time, and research time for Emerald spells is reduced to 1/2 normal (round up & past xp refunded)
    - The cost, training time, and research time for Sapphire spells is reduced to 3/4 normal (round up & past xp refunded)

### Necromancy Initiation Changes

- Upon initiating into Shadowlands Necromancy:
    - You automatically learn **Dispel Shadowlands Necromancy** for free (see Spell Changes below)
- Upon initiating into Labyrinth Necromancy:
    - You automatically learn **Dispel Labyrinth Necromancy** for free (see Spell Changes below)
    - The cost, training time, and research time for Shadowlands spells is reduced to 3/4 normal (round down & past xp refunded)
- Upon Initiating into Void Necromancy:
    - You automatically learn **Dispel Void Necromancy** for free (see Spell Changes below)
    - The cost, training time, and research time for Shadowlands spells is reduced to 1/2 normal (round up & past xp refunded)
    - The cost, training time, and research time for Labyrinth spells is reduced to 3/4 normal (round up & past xp refunded)

### Spell Changes

- **Dispell (Circle) (SorceryType)** functions much as Counterspells, but take a full Cast Sorcery action
- Learning Demon Summoning or Demon Banishment reduces to cost of the other, from the same Circle, by 1/2 (round up)
- **Imbue Amalgam** turns the target into being that follows rules for a half-caste of the sorcerer's type. Essence may not exceed 3.
- **Sworn Brothers' Oath** cannot create an Oathbond greater than 5 unless all members have an Essence Rating above 5, at which point the limit is the lowest Essence rating of its members. Additionally, Oathbonds can be re-cast once per story without costing permanent dots of Willpower or Essence. The 'Familiarity' range is now (Oathbond x 20) yards.
- Summoning Spells may be cast at higher tiers with the following effects:
    - **Demon of the First Circle** as a Sapphire Spell costing 10m more as a 4hr dramatic action any time of day
        - Causes 100yd Localized Calibration lasting (15 x demon's essence rating) min at end of ritual
    - **Demon of the First Circle** as a Adamant Spell costing 20m more as a Shape Sorcery action
        - Causes a **Blasphemy** effect of (demon's essence rating)
    - **Demon of the Second Circle** as an Adamant Spell costing 10m more on any night
        - Causes 500yd Localized Calibration lasting (demon's essence rating /2) hours at end of ritual

## Combat

### Appearance & MDVs, Linguistics & MDVs, Strength & DVs

- Much like how relative Appearance alters DVs for face-to-face Social Attacks and relative Linguistics alters DVs for written Social Attacks... Strength alters DVs for hand-to-hand Physical Attacks. So if a Str3 person is defending against a Str5 person, their DV is effectively 2 less than normal. The most such a differential can affect an MDV or DV is +/- 3.

### XLNCs/Stunts and DVs

- Adding dice to DV via XLNC or Stunt adds to DV _pool_, which is then divided by 2 (round up) to derive DV.

### Speed, Ticks/Rounds, and Weapons

- Speed is no longer an attribute of weapons. Faster weapons have higher rate
- Rounds are 6 ticks long, PCs go first, allied NPCs next, then NPC enemies
- Coordination is still a thing and not assumed automatically
- Speed 3 weapons gain Acc +1 Rate +2
- Speed 4 weapons gain Acc +1 Rate +1
- Speed 5 weapons gain Rate +1

## Crafting

### Craft Ability Changes

- Craft(Fire, Air, Water, Wood, Earth) are all merged into Craft(General) which is the rating used for _**all**_ Craft rolls
- Crafting Artifacts requires a single dot in a Craft sub-Ability for the magical material, such as Craft(Orichalcum), Craft(Jade), etc
- Exotic Crafts like Fate, Genesis, Magitech, Necrotech, Vitriol, Glamour, etc are unlocked with a single dot in their Craft sub-Ability

## Backgrounds

### Sifu

You need a justification for how you learn your Martial Arts Charms in the future, but having a Mentor for only that is eh. Enter the Sifu Background from MoEP:Sidereals p107.

This is open to (and required for) all future learning of Martial Arts. If someone is teaching you MA, you buy them as a Sifu. For _non-Sidereal_ Exalts, the meaning of each rating is a bit different through (see Scroll of Heroes p33 for Mortals):

```
Mortals - Your Sifu Knows:
•     1 TMA
••    2 TMA
•••   3 TMA
••••  5 TMA
••••• 7 TMA

Non-Sid Exalts - Your Sifu Knows:
•     1 TMA, 1 CMA
••    1 TMA, 2 CMA
•••   1 TMA, 3 CMA
••••  1 TMA, 3 CMA, 1 SMA*(partial)
••••• 1 TMA, 4 CMA, 1 SMA*(partial), 1 SMA*

Sidereal Exalts - Your Sifu Knows:
•     1 CMA, 1 SMA(partial)
••    2 CMA, 1 SMA
•••   3 CMA, 2 SMA
••••  5 CMA, 3 SMA
••••• 7 CMA, 4 SMA

A full SMA slot can be converted to 2 CMA slots
A part SMA slot can be converted to 1 CMA slot
A full CMA slot can be converted to 2 TMA slots
A part CMA slot can be converted to 1 TMA slot
And all the above in reverse.

* Sifu knowing any SMA is only with the approval of ST.
```

### Ally

Ally background is unclear about how many dots are needed for each ally that is stronger than a starting character, so I've compiled a list of how allies of various strength should be rated:

```
•     Your Ally is an Essence 2-3 Terrestrial Exalt or Spirit
                or an Essence 2 Celestial Exalt
                or an Essence 2-3 Ghost
••    Your Ally is an Essence 4 Terrestrial Exalt or Spirit
                or an Essence 3-4 Celestial Exalt
                or an Essence 4-5 Ghost
•••   Your Ally is an Essence 5 Exalt or Spirit
                or an Essence 6-7 Ghost
```

Heroic Mortals (even half-breeds like Godbloods, Half-Castes, Ghost-bloods, and Demon-bloods) are Henchmen. Non-heroic are Followers, Retainers, or other such backgrounds.
More powerful allied characters should be taken in a different context, such as Contacts, Sifu, Mentor, or Liege as appropriate.

### Bond-type Backgrounds

Lunar Bond, Karmic Twin, Sworn Brotherhood Devotion (see Spell Changes) all function like Virtue Channels, with the bond rating determining the number of channels. Each usage consumes a single channel, granting a number of dice equal to the full bond rating. Any charm (Moon Follows Sun Assurance) or mechanic (new Story) that would restore dice to this pool instead return channels.

### Familiars

Familiar is a very weak background, especially in light of Demonic Familiar. Starter PCs can kill a Familiar in combat with 0 effort. Even a ‘dangerous’ animal like a bear is no threat to Exalts. In a fight with real danger to the PC, a Familiar would be of little use and be in mortal danger even being there.

- Familiars do not age and can live off of their Master's Essence alone
- Slain Familiars will resurrect after (familiar rating) months at no cost
    - Players may opt to spend 1 permanent wp and a day meditating to resurrect them sooner
        - that permanent wp returns automatically after (familiar rating) months
- All Familiars can communicate with their Master via a telepathic bond of ~1000yds
- All Familiars can speak in Old Realm as part of the magic that empowers them
- All Familiars give a 5mote pool that both Familiar and their Master can pull from
- Familiars gain experience at 1/2 the rate of their Master (ST allocates for PC Familiar)

```
•     a loyal dangerous animal like an intelligent tiger/bear
••    more intelligent and having minor powers (ess1 spirit charms)
•••   full on godblooded creatures or minor spirit (ess2)
••••  a strong spirit (ess3-4) who might have a few tcs spells or tma charms
••••• a construct or powerful spirit (ess5-6) that is famous in its own right
```

## Anima Powers

### Sidereals

- Lesser Signs can be activated with a 5m,1wp surcharge to only be perceivable by effects that allow perception of essence flows, such as All-Encompassing Sorcerer's Sight or Telltale Symphony.

### Twilight/Daybreak/Defiler

> *Replace the text of this anima effect with the following:*
> 
> They may perceive Essence through their anima, allowing them to hone their senses for magic. By spending five motes, they may add (Essence) automatic successes on any (Intelligence + Occult) roll to identify a Charm, **Artifact, or Spell** or to analyze it with Essence sight, and on any (Perception + Awareness) roll made to notice a magical effect or Charm, **Artifact, or Spell**. ~~In addition, this supernatural perception easily pierces through deception, adding a +3 bonus to their Dodge MDV against unnatural Illusions.~~ These effects come into play automatically once they spend 11+ motes of Peripheral Essence.
> 
> For 10 motes, they may touch an Essence 1-4 elemental or a demon of the first circle, conjoining its Essence to her anima. Roll Intelligence + Occult + Essence against the creature’s Dodge MDV. If successful, this creates a pact that changes the spirit into her Familiar, allowing her to target it with applicable Survival Charms. In addition, they can reflexively summon the spirit instantly for three motes, drawing it through the Essence of the world to appear beside her. She may banish the creature again, reflexively, for free, returning it to the tides of Essence that suffuse Creation until needed. They may have up to (Essence-1) spirit familiars bound in this fashion at once.

## Solars

### Generic Charms

- For the purposes of charms requiring two XLNCs, **Infinite (Ability) Mastery**, **Supreme Perfection of (Ability)**, and **(Ability) Essence Flow** count as XLNCs

### Archery/Melee Charms

- **Immaculate Golden Bow** and **Solar Flare Methodology**: buying the second of these two is 1/2 cost
- **Phantom Arrow Technique** is a 1/2 price upgrade of **Essence Arrow Attack**
- **Summoning the Loyal Bow** is a 1/2 price upgrade of **Phantom Arrow Technique**

### Melee/Martial Arts Charms

- Melee: **Call the Blade** also exist under Martial Arts with **Merged** Keyword
- Melee: **Summoning the Loyal Steel** also exist under Martial Arts with **Merged** Keyword
- **Summoning the Loyal Steel** is a 1/2 price upgrade of **Call the Blade**

### War Charms

- **Legendary Warrior Curriculum** is a 1/2 price upgrade of **Tiger Warrior Training Technique**

> **Elite Warrior Cultivation**  
> **Cost**: --; **Mins**: War 5, Essence 5; **Type**: Permanent  
> **Duration**: Permanent  
> **Prerequisite Charms**: Legendary Warrior Curriculum  
> 
> The Solar’s Tiger Warrior Training Technique is permanently enhanced by his supreme knowledge of war. The Solar may now use his training to teach any Supernatural Martial Arts Charms he knows. Additionally, he can now train Willpower to 8, students may benefit from two artificial two-die specialties, and he also abolishes the 4-dot training cap from Tiger Warrior Training Technique and Legendary Warrior Curriculum; only the age, nature, and Essence of his students limits their potential to absorb training under the Solar’s banner. He may also use Tiger Warrior Training Technique to drill soldiers in the use of Supernatural Martial Arts Charms, assuming his students are capable of using such magic.

- **Elite Warrior Cultivation** is a 1/2 price upgrade of **Legendary Warrior Curriculum**

### Integrity Charms

- **Glory to the Most High** does not Exist
- **Shedding Infinite Radiance**, Twilight Effect:
> ~~When her anima flares at the (11+) mote level, the Exalt reduces the cost of all sorcery by half, to a minimum of one mote. This reduction does not apply to necromancy (Daybreak applies to necromancy, not sorcery).~~
> When spending 5m to perceive Essence through their anima, this supernatural perception now easily pierces through deception, adding a +3 bonus to the Solar’s Dodge MDV against unnatural Illusions.
> 
> Additionally for five motes, they can project an aura of pure force, reflexively gaining 5 Soak and 5 Hardness for one turn. At the (11+) mote level, this power activates itself automatically at no cost. The Hardness effect doesn’t stack with other magic/armor that raises Hardness, but the Soak can stack and counts as natural soak.

### Presence Charms

- **Irresistible Salesman Spirit** has **Merged** keyword and is available under Performance as well.

### Occult Charms

- **The Time Is Now** does not exist (see Summoning Spells above).
- **All-Encompassing Sorcerer's Sight** is a 1/2 price upgrade of **Spirit-Detecting Glance**
- **Sorcerer's Burning Chakra Charm** is a 1/2 price upgrade to **All-Encompassing Sorcerer's Sight** and can be purchased a second time to remove the caste mark burn and **Obvious** keyword

### Lore Charms

- **Legendary Scholar Curriculum** is a 1/2 price upgrade of **Harmonious Academic Methodology**
- **Terrestrial Edification Program** does not Enlighten mortals (use **Soul-Enlightening Beneficence** for that). It is a 1/2 price upgrade of **Legendary Scholar Curriculum**
- **Dragon-Soul Enlightening Method** doesn't exist. **Soul-Enlightening Beneficence** has the effect of this charm when used on Dragon-Kings.

> **Ethereal Enlightenment Procedure**  
> **Cost**: --; **Mins**: Lore 6, Essence 6; **Type**: Permanent  
> **Duration**: Permanent  
> **Prerequisite Charms**: Terrestrial Edification Program  
>
> Teachings can be designed to encompass additional lessons as desired by the Lawgiver. In game terms, each training session embeds an Intimacy of the Solar player's choice in addition to the week's training topic. In order to benefit from a week of training augmented this way, a character must also gain the Intimacy coded into the training, and it is fundamentally embedded into her understanding of the lessons. The learned traits may not be used while the character does not harbor the encoded Intimacy. This restriction may be overcome, eventually, by spending an additional 2xp.
>
> At Essence 7, the Solar may use a 4 weeks of training toward the raising his student's Essence traits (up to a max of Solar's Essence-1), but cannot teach someone beyond their capabilities of age and type of being (mortals cannot exceed 3, exalts have age caps, spirits have other requirements to raise essence, etc).

- **Ethereal Enlightenment Procedure** is a 1/2 price upgrade of **Terrestrial Edification Program**

### Awareness Charms

- **Keen (Sense) Technique** costs 1/2 price for additional senses after the first, but Sight, Smell, Taste, Hearing, Touch are all separately purchased. **Unsurpassed (Sense) Technique** is a free upgrade of its prerequisite that unlocks at Awareness 5, Essence 4

## Abyssals

### Generic Charms

- For the purposes of charms requiring two XLNCs, **Infinite (Ability) Mastery**, **Supreme Perfection of (Ability)**, and **(Ability) Essence Flow** count as XLNCs

### Archery/Melee Charms

- **Exquisite Relic Bow** and **Gasp of Dead Gods**: buying the second of these two is 1/2 cost
- **Relic Arrow Method** is a 1/2 price upgrade of **Splinter of the Void**
- **Banished Bow Arsenal** is a 1/2 price upgrade of **Splinter of the Void**

### Melee/Martial Arts Charms

- Melee: **Blade-Summoning Gesture** also exist under Martial Arts with **Merged** Keyword
- Melee: **Void Sheath Technique** also exist under Martial Arts with **Merged** Keyword
- **Void Sheath Technique** is a 1/2 price upgrade of **Call the Blade**

### Integrity Charms

- **World-Ending Void Apostle** does not Exist
- **Void King Ascension**, Daybreak Effect:
> ~~When her anima flares at the (11+) mote level, the Exalt reduces the cost of all sorcery by half, to a minimum of one mote. This reduction does not apply to necromancy (Daybreak applies to necromancy, not sorcery).~~
> When spending 5m to perceive Essence through their anima, this supernatural perception now easily pierces through deception, adding a +3 bonus to the Solar’s Dodge MDV against unnatural Illusions.
> 
> Additionally for five motes, they can project an aura of pure force, reflexively gaining 5 Soak and 5 Hardness for one turn. At the (11+) mote level, this power activates itself automatically at no cost. The Hardness effect doesn’t stack with other magic/armor that raises Hardness, but the Soak can stack and counts as natural soak.

### Occult Charms

- **Through Dead Eyes** is a 1/2 price upgrade of **Spirit-Sensing Meditation**
- A Mirror of **Sorcerer's Burning Chakra Charm** called **Eye of Oblivion** exists as a 1/2 price upgrade to **Through Dead Eyes** and can be purchased a second time to remove the caste mark bleeding and **Obvious** keyword. **Eye of Oblivion** has **Avatar(1)** keyword.

### Awareness Charms

- **Superior (Sense) Focus** costs 1/2 price for additional senses after the first, but Sight, Smell, Taste, Hearing, Touch are all separately purchased. **Entropic Awakening of (Sense)** is a free upgrade of its prerequisite that unlocks at Awareness 5, Essence 4

## Infernals

### Generic Charms

> **Ascendancy Mantle of (Yozi)**  
> _Replace this Charm in its entirety with the following:_

> **Ascendant (Yozi) Intuition**  
> **Cost**: --; **Mins**: Essence 5; **Type**: Permanent  
> **Keywords**: None  
> **Duration**: Permanent  
> **Prerequisite Charms**: First (Yozi) Excellency
> 
> Your affinity for charms of this Yozi allows you to lower the Essence requirement for Charms of this Yozi by 1.

- **(Yozi) Yozi-Body Unity** and **(Yozi) Cosmic Principle** are not PC purchasable. All Charms that rely on them are not PC purchasable either.

### Ophidian Charms

- **Loom-Snarling Deception**: Exalts can maintain (Essence) different false destinies by committing 1m when deactivating the charm. Permanently lose a destiny by uncommitting all motes. Exact destinies can never be recreated again, though similar ones may. Re-donning a previously created and preserved identity costs 1m less due to the already committed 1m. Akuma who learn this charm _always_ consume one of their false destiny slots with their former selves, but it costs 0m to maintain, but don't save that 1m when activating the charm.
- **Witness to Darkness**: Ignore the -1 internal penalty.

### Adorjani Charms

- **Eloquence of Unspoken Words** functions with any language and it's radius can be controlled anywhere up to the defined radius.

## Sidereals

### Generic Charms

## Lunars

### Generic Charms

- For the purposes of charms requiring two XLNCs, **Flawless (Attribute) Focus** and **Instinctive (Attribute) Unity** count as XLNCs

> **Absolute (Attribute) Prowess**  
> **Cost**: --; **Mins**: (Attribute) 5, Essence 4; **Type**: Permanent  
> **Keywords**: None  
> **Duration**: Permanent  
> **Prerequisite Charms**: Instinctive (Attribute) Unity, Flawless (Attribute) Focus
> 
> Lunar Exalts can push the boundaries of their own natural potential to become true exemplars of (Attribute). This charm increases the maximum motes the character may spend on their (Attribute) Excellencies to (Attribute + Relevant Attribute Specialty). This charm is incompatible with any other effects that increase the maximum benefit (such as Relentless Lunar Fury, Irresistible Silver Spirit, and Inevitable Genius Insight) and only the greatest calculation is used. Additionally, this charm removes the limit on motes able to be committed towards Instinctive (Attribute) Unity.
> 
> At Essence 5, allows the Lunar may purchase Charms with an (Attribute) and Essence requirement one higher than normal.

### Essence Charms

- All-Encompassing Sorcerer's Sight for Lunars is called **Essence-Aroma Understanding** and functions on smell. It is a 1/2 price upgrade of **Instinctive Essence Prediction** (does not affect **Fury-OK** effect).
### Perception Advantage Charms
- **Keen (Sense) Technique** costs 1/2 price for additional senses after the first, but Sight, Smell, Taste, Hearing, Touch are all separately purchased. **Heightened (Sense) Method** is a free upgrade of its prerequisite that unlocks at Perception 4, Essence 4

### Craft Charms

> **Storing the Sacred Silver**  
> **Cost**: Varies; **Mins**: Dexterity 5, Essence 4; **Type**: Reflexive (Step 1)  
> **Keywords**: Obvious  
> **Duration**: Indefinite  
> **Prerequisite Charms**: Lunar Blade Reconfiguration
> 
> This charm allows the Steward to store moonsilver artifacts with which they are attuned within their own tattoos where they can keep them held until they are needed. Only artifacts comprised mainly of moonsilver may be stored this way (as a general rule of thumb, an artifact weapon or armor should be able to provide a magical material bonus to the bearer). The cost to store an artifact this way is equal to its rating in motes, and artifacts may be retrieved by a second activation of this charm, returning to the position they were stored from (Armor is donned, a weapon is returned to hand, etc.). Lunars with Changing Plumage Mastery may make cosmetic changes to artifacts stored and recalled this way, but the artifacts lose these customizations should they become de-attuned.
> 
> Items stored this way do not provide their normal benefits until they are recalled.

### Knacks
- **Lightning-Change Style** is a 1/2 price upgrade of **Quicksilver Second Face**
- **Constant Quicksilver Rearrangement** is a 1/2 price upgrade of **Lightning-Change Style**

## Exalted Apparent Age

- Exalts age towards their ideal apparent age (for many that is looking to be in their ~25-30s, always clearly an adult) post-exaltation. Examples:
	- A girl who exalts at 15years old continues to age normally until she reaches her ideal apparent age.
	- A 60 year old man begins aging backwards for years until he looks like his ideal apparent age again.
- This ideal apparent age is very much mental and can change over time, but their apparent age will only move towards the new ideal by 1 year each year without specialized powers.
- Once they reach the last half-century (final century for DBs) or so of their lifespan their apparent age begins rising, so when they die of old age, they looked like distinguished 75-80 year olds.
